package co.com.choucair.bancolombia2.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;
@RunWith(CucumberWithSerenity.class)
@CucumberOptions (features = "src/test/resources/features/bancolombia2.feature",
        tags = "@historiausuario",
        glue = "co.com.choucair.bancolombia2.stepdefinitions",
        snippets = SnippetType.CAMELCASE )

public class RunnerTags {
}