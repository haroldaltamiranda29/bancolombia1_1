package co.com.choucair.bancolombia2.stepdefinitions;

import co.com.choucair.bancolombia2.model.ValoresSegunCuotaAPagar;
import co.com.choucair.bancolombia2.model.ValoresSegunVivienda;
import co.com.choucair.bancolombia2.questions.CuotaMensual;
import co.com.choucair.bancolombia2.questions.SegunValor;
import co.com.choucair.bancolombia2.tasks.*;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;
import java.util.List;

public class Bancolombia2StepDefinitions {

    @Before
    public void setStage (){
        OnStage.setTheStage(new OnlineCast());
    }

    @Dado("^que estoy en la pagina bancolombia personas,doy click en produtos y servicios$")
    public void queEstoyEnLaPaginaBancolombiaPersonasDoyClickEnProdutosYServicios(){
        OnStage.theActorCalled("Grupo2").wasAbleTo(Abrir.grupoBancolombia(), Oprimir.productosServicios());
    }

    @Y("^doy click en leasing luego click en leasing habitacional$")
    public void doyClickEnLeasingLuegoClickEnLeasingHabitacional(){
        OnStage.theActorInTheSpotlight().attemptsTo(Learsing.leasingLeasigHabitacional());
    }

    @Y("^doy click en ir al simulador$")
    public void doyClickEnIrAlSimulador(){
        OnStage.theActorInTheSpotlight().attemptsTo(Ingresar.irAlSimulador());
    }

    @Cuando("^seleccciono la opcion segun el valor de la vivienda$")
    public void selecccionoLaOpcionSegunElValorDeLaVivienda(){
        OnStage.theActorInTheSpotlight().attemptsTo(Seleccionar.valorVivienda());
    }

    @Y("^ingreso el valorActivo, plazo y click en simular$")
    public void ingresoElValorActivoPlazoYClickEnSimular(List<ValoresSegunVivienda> ValoresSegunVivienda){
        OnStage.theActorInTheSpotlight().attemptsTo(Diligenciar.datosDeEntrada(ValoresSegunVivienda));
    }

    @Entonces("^puedo ver la simulacion con el valor a prestar y la cuotaMensual$")
    public void puedoVerLaSimulacionConElValorAPrestarYLaCuotaMensual(List<ValoresSegunVivienda> ValoresSegunVivienda){
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(CuotaMensual.valorVivienda(ValoresSegunVivienda), Matchers.equalTo("$ 335.145")));
    }

    @Cuando("^selecciono la opcion segun el valor de la cuota que puedo pagar$")
    public void seleccionoLaOpcionSegunElValorDeLaCuotaQuePuedoPagar(){
        OnStage.theActorInTheSpotlight().attemptsTo(Opcion.cuotaQuePuedoPagar());
    }

    @Y("^e ingreso el valorQuePuedoPagar cantidadAÃ±os  click en simular$")
    public void eIngresoElValorQuePuedoPagarCantidadAÃOsClickEnSimular(List<ValoresSegunCuotaAPagar> ValoresSegunCuotaPagar){
        OnStage.theActorInTheSpotlight().attemptsTo(Llenar.datosDeEntrada(ValoresSegunCuotaPagar));
    }

    @Entonces("^puedo ver la cantidadPrestamos$")
    public void puedoVerLaCantidadPrestamos(List<ValoresSegunCuotaAPagar> ValoresSegunCuotaAPagar){
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(SegunValor.loQuePuedoPagar(ValoresSegunCuotaAPagar),Matchers.equalTo("$ 34.554.266")));
    }
}
