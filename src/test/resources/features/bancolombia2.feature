#Autor: Grupo2
# language: es
  @historiausuario
  Característica: Bancolombia, como un usuario quiero ingresar a Bancolombia a verificar
    el funcionamiento de la pantalla de simulacion de Canon Financiero
  Antecedentes:
    Dado que estoy en la pagina bancolombia personas,doy click en produtos y servicios
    Y doy click en leasing luego click en leasing habitacional
    Y doy click en ir al simulador
  @CP1
    Escenario:Simular el valor del prestamo y cuota mensual segun el valor de la vivienda
      Cuando seleccciono la opcion segun el valor de la vivienda
      Y ingreso el valorActivo, plazo y click en simular
        |valorActivo   |plazo|
        |36341040     |10   |
      Entonces puedo ver la simulacion con el valor a prestar y la cuotaMensual
        | cuotaMensual |
        |$ 335.145     |
    @CP2
    Escenario: Simular el valor del prestamo segun el valor de cuota que puedo pagar
      Cuando selecciono la opcion segun el valor de la cuota que puedo pagar
      Y e ingreso el valorQuePuedoPagar cantidadAÃ±os  click en simular
        |valorMensual   |plazo|
        |36341040     |10   |
      Entonces puedo ver la cantidadPrestamos
        | cantidadPrestamos |
        |$ 34.554.266 |
