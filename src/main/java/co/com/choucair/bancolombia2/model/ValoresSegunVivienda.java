package co.com.choucair.bancolombia2.model;

public class ValoresSegunVivienda {
    private String valorActivo;
    private String plazo;
    private String cuotaMensual;

    public String getCuotaMensual() {
        return cuotaMensual;
    }

    public void setCuotaMensual(String cuotaMensual) {
        this.cuotaMensual = cuotaMensual;
    }

    public String getValorActivo() {
        return valorActivo;
    }

    public void setValorActivo(String valorActivo) {

        this.valorActivo = valorActivo;
    }

    public String getPlazo() {

        return plazo;
    }

    public void setPlazo(String plazo) {

        this.plazo = plazo;
    }


}
