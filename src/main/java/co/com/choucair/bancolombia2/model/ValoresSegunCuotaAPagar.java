package co.com.choucair.bancolombia2.model;

public class ValoresSegunCuotaAPagar {

    private String cantidadPrestamos;
    private String valorMensual;
    private String plazo;

    public String getValorMensual() {
        return valorMensual;
    }

    public void setValorMensual(String valorMensual) {
        this.valorMensual = valorMensual;
    }

    public String getPlazo() {
        return plazo;
    }

    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    public String getCantidadPrestamos() {
        return cantidadPrestamos;
    }

    public void setCantidadPrestamos(String cantidadPrestamos) {
        this.cantidadPrestamos = cantidadPrestamos;
    }

}
