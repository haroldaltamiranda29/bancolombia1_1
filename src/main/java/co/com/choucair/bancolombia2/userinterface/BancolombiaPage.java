package co.com.choucair.bancolombia2.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class BancolombiaPage {
    public static final Target PRODUCTOS_SERVICIOS = Target.the("Selecciona productos y servicios")
            .located(By.id("menu-productos"));
    public static final Target LEARSING = Target.the("Selecciona learsing")
            .located(By.id("header-productos-leasing"));
    public static final Target LEARSING_HABITACIONAL = Target.the("Selecciona Learsing Habitacional")
            .located(By.xpath("//*[@id=\"category-detail\"]/div/div/div[2]/div/div[2]/h2/a"));
    public static final Target IR_AL_SIMULADOR = Target.the("Click en la boton Ir al simuladr")
            .located(By.xpath("//*[@id=\"menu-sticky\"]/div/div/div[1]/div[6]"));
    public static final Target OPCION_VALOR_VIVIENDA = Target.the("Selecciona Segun el valor de la vivienda")
            .located(By.xpath("//*[@id=\"calcular-cuotas\"]/div"));
    public static final Target VALOR_ACTIVO = Target.the("Digita el valor comercial de la vivienda")
            .located(By.id("valor-simulacion"));
    public static final Target PLAZO = Target.the("Digita el plazo en años")
            .located(By.id("valor-year"));
    public static final Target FECHA_NACIMIENTO = Target.the("Click en el campo de la fecha")
            .located(By.id("campo-fecha"));
    public static final Target AÑO = Target.the("Selecciona el año de nacimiento")
            .located(By.xpath("//*[@id=\"mat-datepicker-0\"]/div/mat-multi-year-view/table/tbody/tr[5]/td[4]"));
    public static final Target MES = Target.the("Selecciona el mes de nacimiento")
            .located(By.xpath("//*[@id=\"mat-datepicker-0\"]/div/mat-year-view/table/tbody/tr[3]/td[2]"));
    public static final Target DIA = Target.the("Selecciona el dia de nacimiento")
            .located(By.xpath("//*[@id=\"mat-datepicker-0\"]/div/mat-month-view/table/tbody/tr[4]/td[2]"));
    public static final Target BOTON_SIMULAR = Target.the("Click en el boton Simular")
            .located(By.id("boton-simular"));
    public static final Target CUOTA_MENSUAL = Target.the("valor de la cuota mensual")
            .located(By.id("resultado-FIXEDFEECOP"));
    public static final Target OPCION_CUOTA_QUE_PUEDO_PAGAR = Target.the("Selecciona segun el valor de la cuota que puedo pagar")
            .located(By.xpath("//*[@id=\"cuanto-prestar\"]/div"));
    public static final Target CUANTO_PUEDO_PAGAR = Target.the("Digita el valor que puedes pagar mensual")
            .located(By.xpath("//*[@id=\"valor-simulacion\"]"));
    public static final Target PLAZO_AÑOS = Target.the("Digita el plazo en años")
            .located(By.id("valor-year"));
}

