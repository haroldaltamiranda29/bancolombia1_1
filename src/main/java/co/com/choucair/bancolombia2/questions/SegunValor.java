package co.com.choucair.bancolombia2.questions;

import co.com.choucair.bancolombia2.model.ValoresSegunCuotaAPagar;
import co.com.choucair.bancolombia2.userinterface.BancolombiaPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

public class SegunValor implements Question<String> {

    private List<ValoresSegunCuotaAPagar> ValoresSegunCuotaAPagar;

    public SegunValor(List<co.com.choucair.bancolombia2.model.ValoresSegunCuotaAPagar> valoresSegunCuotaAPagar) {
        this.ValoresSegunCuotaAPagar = valoresSegunCuotaAPagar;
    }

    public static SegunValor loQuePuedoPagar(List<ValoresSegunCuotaAPagar> valoresSegunCuotaAPagar) {
        return new SegunValor(valoresSegunCuotaAPagar);
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(BancolombiaPage.CUOTA_MENSUAL).viewedBy(actor).asString();
    }
}
