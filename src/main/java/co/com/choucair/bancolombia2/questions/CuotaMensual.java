package co.com.choucair.bancolombia2.questions;

import co.com.choucair.bancolombia2.model.ValoresSegunVivienda;
import co.com.choucair.bancolombia2.userinterface.BancolombiaPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import java.util.List;

public class CuotaMensual implements Question<String> {

    private List<ValoresSegunVivienda> valoresSegunVivienda;

    public CuotaMensual(List<ValoresSegunVivienda> valoresSegunVivienda) {
        this.valoresSegunVivienda = valoresSegunVivienda;
    }

    public static CuotaMensual valorVivienda(List<ValoresSegunVivienda> valoresSegunVivienda) {
        return new CuotaMensual(valoresSegunVivienda);
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(BancolombiaPage.CUOTA_MENSUAL).viewedBy(actor).asString();
    }
}
