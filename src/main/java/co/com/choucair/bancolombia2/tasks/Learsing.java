package co.com.choucair.bancolombia2.tasks;

import co.com.choucair.bancolombia2.userinterface.BancolombiaPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class Learsing implements Task {
    public static Learsing leasingLeasigHabitacional() {
        return Tasks.instrumented(Learsing.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.wasAbleTo(
                Click.on(BancolombiaPage.LEARSING),
                Click.on(BancolombiaPage.LEARSING_HABITACIONAL)
        );
    }
}
