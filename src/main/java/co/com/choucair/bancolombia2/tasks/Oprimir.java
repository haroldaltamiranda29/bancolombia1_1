package co.com.choucair.bancolombia2.tasks;

import co.com.choucair.bancolombia2.userinterface.BancolombiaPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class Oprimir implements Task {
    public static Oprimir productosServicios() {
        return Tasks.instrumented(Oprimir.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.wasAbleTo(
                Click.on(BancolombiaPage.PRODUCTOS_SERVICIOS)
        );
    }
}
