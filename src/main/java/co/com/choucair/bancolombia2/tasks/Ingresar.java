package co.com.choucair.bancolombia2.tasks;

import co.com.choucair.bancolombia2.userinterface.BancolombiaPage;
import cucumber.api.java.sl.In;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class Ingresar implements Task {
    public static Ingresar irAlSimulador() {
        return Tasks.instrumented(Ingresar.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.wasAbleTo(
                Click.on(BancolombiaPage.IR_AL_SIMULADOR)
        );
    }
}
