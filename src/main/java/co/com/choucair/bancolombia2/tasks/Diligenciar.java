package co.com.choucair.bancolombia2.tasks;

import co.com.choucair.bancolombia2.model.ValoresSegunVivienda;
import co.com.choucair.bancolombia2.userinterface.BancolombiaPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

public class Diligenciar implements Task {

    List<ValoresSegunVivienda> ValoresSegunVivienda;

    public Diligenciar(List<ValoresSegunVivienda> valoresSegunVivienda) {
        ValoresSegunVivienda = valoresSegunVivienda;
    }

    public static Diligenciar datosDeEntrada(List<ValoresSegunVivienda> ValoresSegunVivienda) {
        return Tasks.instrumented(Diligenciar.class,ValoresSegunVivienda);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(ValoresSegunVivienda.get(0).getValorActivo()).into(BancolombiaPage.VALOR_ACTIVO),
                Enter.theValue(ValoresSegunVivienda.get(0).getPlazo()).into(BancolombiaPage.PLAZO),
                Click.on(BancolombiaPage.FECHA_NACIMIENTO),
                Click.on(BancolombiaPage.AÑO),
                Click.on(BancolombiaPage.MES),
                Click.on(BancolombiaPage.DIA),
                Click.on(BancolombiaPage.BOTON_SIMULAR)
        );
        try {
            Thread.sleep (10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
