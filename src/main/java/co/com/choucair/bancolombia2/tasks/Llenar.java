package co.com.choucair.bancolombia2.tasks;

import co.com.choucair.bancolombia2.model.ValoresSegunCuotaAPagar;
import co.com.choucair.bancolombia2.userinterface.BancolombiaPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

public class Llenar implements Task {

    List<ValoresSegunCuotaAPagar> ValoresSegunCuotaAPagar;
    public static Llenar datosDeEntrada(List<ValoresSegunCuotaAPagar> valoresSegunCuotaPagar) {
        return Tasks.instrumented(Llenar.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        System.out.println("Entro");
        actor.attemptsTo(
                Enter.theValue("36341040").into(BancolombiaPage.CUANTO_PUEDO_PAGAR),
                Enter.theValue("10").into(BancolombiaPage.PLAZO_AÑOS),
                Click.on(BancolombiaPage.FECHA_NACIMIENTO),
                Click.on(BancolombiaPage.AÑO),
                Click.on(BancolombiaPage.MES),
                Click.on(BancolombiaPage.DIA),
                Click.on(BancolombiaPage.BOTON_SIMULAR)
        );
        try {
            Thread.sleep (20000);
            System.out.println("paso");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
