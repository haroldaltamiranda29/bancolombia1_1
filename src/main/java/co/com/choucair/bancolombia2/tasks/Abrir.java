package co.com.choucair.bancolombia2.tasks;

import co.com.choucair.bancolombia2.userinterface.Bancolombia2Page;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task {

    private Bancolombia2Page Bancolombia2Page;

    public static Abrir grupoBancolombia() {
        return Tasks.instrumented(Abrir.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.wasAbleTo(
                Open.browserOn(Bancolombia2Page)
        );
    }
}
